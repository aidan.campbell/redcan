This code is designed to do a handful of simple tasks to light up any local process scanner

1. Create a child process
   - The default will only work in linux-like systems, as the subprocess is a callout to the standard `sleep` binary.  It inherits the environment variables of this application, but is otherwise left alone.  No signals are exchanged after forking.
2. Create, modify and delete a file
   - This is pretty straightforward. Just creates a new file in the OS's standard temp dir, puts some bytes into it, then deletes it
3. Establish a network connection and transfer data
   - This is a bit more nuanced: an HTTP GET request is performed, which ultimately triggers 3 requests: DNS A and AAAA lookups, followed by the HTTP GET request.
   - Omitting the AAAA lookup is possible, but requires the relatively ugly approach of manually resolving the host then creating the HTTP request with the proper `Host` header.  This does not work in all cases (e.g. Cloudflare doesn't like this).

During each of these steps, JSON-formatted logs are emitted to stdout.

To aid in this functionality, three libraries are pulled in:
   - `logrus`: used for logging.  It's not being used for much, but I'm familiar with it.  Some decorators around the stdlib `log` package could achieve the same result.
   - `gopsutil`: used for getting data about processes.  To my knowledge, Go doesn't provide a platform-independent interface for retrieving details like an arbitrary process's user or start time.  There are numerous workarounds for this (e.g. raw syscalls chosen by OS type or orchestrating `ps` or reading `/proc`).  `gopsutil` easily solves this.
   - `cobra`: used for command line flag parsing.  The standard library's `flag` library would suffice, but it isn't well suited for more complicated usage like subcommands, similar to the `log` package.

Usage (via `go run . --help`)
```
redcan: a binary for triggering basic interactions in linux or linux-like environments. By default all three subcommands of `proc`, `file`, and `network` will be run

Usage:
  redcan [flags]
  redcan [command]

Available Commands:
  file        trigger basic filesystem changes
  help        Help about any command
  network     trigger an HTTP request
  proc        trigger creation of a subprocess

Flags:
  -p, --abspath string      path of file to create/modify/execute. Defaults to a file named "redcan" in the OS's temp dir/"$TMPDIR" (default "/var/folders/4h/7vd0bcbj0lqftxr1lzqrvk0r0000gn/T/redcan")
  -c, --create              whether the file should be created (default true)
  -m, --modify              whether the file should be modified (default true)
  -d, --delete              whether the file should be deleted (default true)
  -e, --executable string   absolute path of the executable to run in the subprocess (default "/bin/sleep")
  -a, --args stringArray    arguments to pass to the executable (default [100])
  -h, --hostname string     hostname where an HTTP GET request should be sent (default "google.com")
      --help                print this help message

Use "redcan [command] --help" for more information about a command.

```