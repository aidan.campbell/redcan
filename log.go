package main

import (
	"github.com/shirou/gopsutil/v3/process"
	"github.com/sirupsen/logrus"
	"os"
	"strconv"
	"strings"
	"time"
)

// customLogger adds default fields to all logs emitted: these are all process-related fields
type customLogger struct {
	formatter logrus.Formatter
}

func (l customLogger) Format(entry *logrus.Entry) ([]byte, error) {

	proc, err := process.NewProcess(int32(os.Getpid()))
	if err != nil {
		return nil, err
	}

	createTimeMS, err := proc.CreateTime()
	if err != nil {
		return nil, err
	}

	username, err := proc.Username()
	if err != nil {
		return nil, err
	}

	name, err := proc.Name()
	if err != nil {
		return nil, err
	}

	createTime := time.Unix(createTimeMS/1000, createTimeMS%1000)
	entry.Data["proc_start"] = createTime.Format(time.RFC3339Nano)
	entry.Data["proc_username"] = username
	entry.Data["proc_name"] = name
	entry.Data["proc_command_line"] = strings.Join(os.Args, " ")
	entry.Data["proc_id"] = strconv.Itoa(os.Getpid())

	return l.formatter.Format(entry)
}
