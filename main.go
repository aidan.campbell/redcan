package main

import (
	"github.com/raidancampbell/redcan/file"
	"github.com/raidancampbell/redcan/network"
	"github.com/raidancampbell/redcan/proc"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"os"
	"path/filepath"
	"time"
)

func init() {
	// set up the log formatting
	logrus.SetFormatter(customLogger{&logrus.JSONFormatter{TimestampFormat: time.RFC3339Nano}})

	// set the flags as persistent against the root command: this allows them to be inherited into subcommands
	// and therefore allows invocation as `redcan -h google.com` or `redcan net -h google.com`
	rootCmd.PersistentFlags().StringP("abspath", "p", filepath.Join(os.TempDir(), "redcan"), "path of file to create/modify/execute. Defaults to a file named \"redcan\" in the OS's temp dir/\"$TMPDIR\"")
	rootCmd.PersistentFlags().BoolP("create", "c", true, "whether the file should be created")
	rootCmd.PersistentFlags().BoolP("modify", "m", true, "whether the file should be modified")
	rootCmd.PersistentFlags().BoolP("delete", "d", true, "whether the file should be deleted")

	rootCmd.PersistentFlags().StringP("executable", "e", "/bin/sleep", "absolute path of the executable to run in the subprocess")
	rootCmd.PersistentFlags().StringArrayP("args", "a", []string{"100"}, "arguments to pass to the executable")

	rootCmd.PersistentFlags().StringP("hostname", "h", "google.com", "hostname where an HTTP GET request should be sent")
	rootCmd.PersistentFlags().Bool("help", false, "print this help message")

	// the flags are logically grouped, don't sort them alphabetically in the help text
	rootCmd.Flags().SortFlags = false
	rootCmd.PersistentFlags().SortFlags = false

	// don't offer to install shell completions for this
	rootCmd.CompletionOptions.DisableDefaultCmd = true
}

var rootCmd = &cobra.Command{
	Use:  "redcan",
	Long: "redcan: a binary for triggering basic interactions in linux or linux-like environments. By default all three subcommands of `proc`, `file`, and `network` will be run",
	Run: func(cmd *cobra.Command, args []string) {
		// do the process things
		proc.Trigger.Run(cmd, args)

		// do the file things
		file.Trigger.Run(cmd, args)

		// do the network things
		network.Trigger.Run(cmd, args)
	},
}

func main() {
	rootCmd.AddCommand(file.Trigger, proc.Trigger, network.Trigger)
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}
