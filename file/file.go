// Package file covers file creation, modification, and deletion.
// During its functionality, it should log the following details:
// - Timestamp of activity
// - Full path to the file
// - Activity descriptor - e.g. create, modified, delete
// - Username that started the process that created/modified/deleted the file
// - Process name that created/modified/deleted the file
// - Process command line
// - Process ID
package file

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"os"
	"path/filepath"
	"time"
)

var Trigger = &cobra.Command{
	Use:     "file",
	Aliases: []string{"fs"},
	Short:   "trigger basic filesystem changes",
	Long:    "create, modify, and delete an optionally specified file",
	Run: func(cmd *cobra.Command, args []string) {
		path, _ := cmd.Flags().GetString("abspath")

		// if the user gave a relative path, we should convert it into an absolute path for the sake of correct logs
		absPath, err := filepath.Abs(path)
		if err != nil {
			logrus.WithError(err).WithField("path", path).Error("given file path could not be converted into absolute path")
			os.Exit(1)
		}

		shouldCreate, _ := cmd.Flags().GetBool("create")
		shouldModify, _ := cmd.Flags().GetBool("modify")
		shouldDelete, _ := cmd.Flags().GetBool("delete")

		if shouldCreate {
			err := create(absPath)
			if err != nil {
				os.Exit(1)
			}
			// sleep between each action so that logs have usefully unique timestamps
			time.Sleep(100 * time.Millisecond)
		}

		if shouldModify {
			err := modify(absPath)
			if err != nil {
				os.Exit(1)
			}
			time.Sleep(100 * time.Millisecond)
		}

		if shouldDelete {
			err := del(absPath)
			if err != nil {
				os.Exit(1)
			}
			time.Sleep(100 * time.Millisecond)
		}
	},
}

// create will create a file at the given path with no contents
// it will then emit a log message containing the following:
// timestamp, full path, created, process username, process name, process commands, and the process ID
func create(absPath string) error {
	newFile, err := os.Create(absPath)
	if err != nil {
		logrus.WithError(err).WithField("absolute_path", absPath).Error("unable to create file")
		return err
	}
	if err := newFile.Close(); err != nil {
		logrus.WithError(err).WithField("absolute_path", absPath).Error("unable to close created file")
		return err
	}
	emitLog(absPath, "created")
	return nil
}

// modify will modify the file given file to add some contents
// it will then emit a log message containing the following:
// timestamp, full path, modified, process username, process name, process commands, and the process ID
func modify(absPath string) error {
	flags := os.O_WRONLY | os.O_APPEND
	file, err := os.OpenFile(absPath, flags, 0640)
	if err != nil {
		logrus.WithError(err).WithField("absolute_path", absPath).Error("unable to open file for modification")
		return err
	}

	_, err = file.WriteString("red canary")
	if err != nil {
		logrus.WithError(err).WithField("absolute_path", absPath).Error("unable to write data to file")
		return err
	}

	err = file.Close()
	if err != nil {
		logrus.WithError(err).WithField("absolute_path", absPath).Error("unable to close modified file")
		return err
	}

	emitLog(absPath, "modified")
	return nil
}

// del will delete the file given file
// it will then emit a log message containing the following:
// timestamp, full path, deleted, process username, process name, process commands, and the process ID
func del(absPath string) error {
	err := os.Remove(absPath)
	if err != nil {
		logrus.WithError(err).WithField("absolute_path", absPath).Error("unable to delete file")
		return err
	}

	emitLog(absPath, "deleted")
	return nil
}

func emitLog(absPath, activity string) {
	logrus.WithFields(logrus.Fields{
		"absolute_path": absPath,
	}).Infof("file has been %s", activity)
}
