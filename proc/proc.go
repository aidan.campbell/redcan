// Package proc covers the creation of a new process.
// During its functionality, it should log the following details:
// - Timestamp of start time
// - Username that started the process
// - Process name
// - Process command line
// - Process ID
package proc

import (
	"github.com/shirou/gopsutil/v3/process"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"os"
	"strconv"
	"time"
)

var Trigger = &cobra.Command{
	Use:     "proc",
	Aliases: []string{"process"},
	Short:   "trigger creation of a subprocess",
	Long:    "fork off a subprocess to perform an optionally specified command",
	Run: func(cmd *cobra.Command, args []string) {
		exec, _ := cmd.Flags().GetString("executable")
		execArgs, _ := cmd.Flags().GetStringArray("args")

		err := Create(exec, execArgs...)
		if err != nil {
			os.Exit(1)
		}
		time.Sleep(100 * time.Millisecond)
	},
}

// Create will create a child process, and log the following fields:
// - Timestamp of start time
// - Username that started the process
// - Process name
// - Process command line
// - Process ID
func Create(exec string, args ...string) error {
	procArgv := append([]string{exec}, args...)
	attr := &os.ProcAttr{
		Dir:   ".",
		Env:   os.Environ(),
		Files: nil, // does not support communication via stdin/out/err: currently no need
		Sys:   nil, // no OS-specific attrs
	}
	childProc, err := os.StartProcess(exec, procArgv, attr)
	if err != nil {
		logrus.WithError(err).WithField("proc_argv", procArgv).Error("unable to create child process")
		return err
	}
	return emitLog(childProc.Pid)
}

func emitLog(pid int) error {

	proc, err := process.NewProcess(int32(pid))
	if err != nil {
		return err
	}
	createTimeMS, err := proc.CreateTime()
	if err != nil {
		return err
	}
	username, err := proc.Username()
	if err != nil {
		return err
	}
	name, err := proc.Name()
	if err != nil {
		return err
	}
	cmdLine, err := proc.Cmdline()
	if err != nil {
		return err
	}

	createTime := time.Unix(createTimeMS/1000, createTimeMS%1000)
	logrus.WithFields(logrus.Fields{
		"child_proc_start":        createTime.Format(time.RFC3339Nano),
		"child_proc_username":     username,
		"child_proc_name":         name,
		"child_proc_command_line": cmdLine,
		"child_proc_id":           strconv.Itoa(pid),
	}).Info("child process created")
	return nil
}
