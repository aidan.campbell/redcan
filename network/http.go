// Package network establishes a connection to another service and transfers data over the connection.
// During its functionality, it should log the following details:
// - Timestamp of activity
// - Username that started the process that initiated the network activity
// - Destination address and port
// - Source address and port
// - Amount of data sent
// - Protocol of data sent
// - Process name
// - Process command line
// - Process ID
package network

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"net"
	"net/http"
	"os"
	"time"
)

var Trigger = &cobra.Command{
	Use:     "network",
	Aliases: []string{"net"},
	Short:   "trigger an HTTP request",
	Long:    "trigger an HTTP GET request to optionally specified hostname",
	Run: func(cmd *cobra.Command, args []string) {
		host, _ := cmd.Flags().GetString("hostname")
		err := HTTPRequest(host)
		if err != nil {
			os.Exit(1)
		}
		time.Sleep(100 * time.Millisecond)
	},
}

// HTTPRequest will resolve and initiate an HTTP GET request to the given host.
//This will result in two UDP DNS queries (A and AAAA), and a single TCP HTTP GET request
func HTTPRequest(hostname string) error {

	// form the HTTP request, and declare that we should close the connection when we're done
	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("http://%s:80/", hostname), nil)
	if err != nil {
		logrus.WithError(err).WithField("hostname", hostname).Error("unable to construct HTTP request")
		return err
	}
	req.Close = true

	// build the client to send the HTTP request:
	// for simplicity, don't follow redirects
	// and hook into the dialer so that we can observe and log both the DNS and HTTP connections
	client := http.DefaultClient
	client.CheckRedirect = func(req *http.Request, via []*http.Request) error {
		return http.ErrUseLastResponse
	}
	client.Transport = &http.Transport{
		DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
			d := &net.Dialer{
				Resolver: measuringResolver,
			}

			c, err := d.DialContext(ctx, network, addr)
			return &loggingConnection{
				transport: network,
				Conn:      c,
			}, err
		},
	}

	// actually do the HTTP request: All network activity will happen here
	do, err := client.Do(req)
	if err != nil {
		logrus.WithError(err).WithField("hostname", hostname).Error("error encountered when executing HTTP request")
		return err
	}

	// close the body: not strictly necessary here, but it's good practice
	err = do.Body.Close()
	if err != nil {
		logrus.WithError(err).WithField("hostname", hostname).Error("error encountered while closing HTTP response body")
		return err
	}

	return nil
}
