package network

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"net"
	"time"
)

var (
	// measuringResolver is the default DNS resolver that uses the loggingConnection to log incoming/outgoing byte counts
	measuringResolver = &net.Resolver{
		PreferGo: true,
		Dial: func(ctx context.Context, network, addr string) (net.Conn, error) {
			d := &net.Dialer{}
			c, err := d.DialContext(ctx, network, addr)
			return &loggingConnection{
				transport: network,
				Conn:      c,
			}, err
		},
	}
)

// loggingConnection implements the net.Conn and net.PacketConn interfaces
var _ net.Conn = (*loggingConnection)(nil)
var _ net.PacketConn = (*loggingConnection)(nil)

type loggingConnection struct {
	net.Conn
	transport string
}

func (l *loggingConnection) emitLog(nBytes int, msg string) {
	logrus.WithFields(logrus.Fields{
		"net_dest":          l.RemoteAddr().String(),
		"net_source":        l.LocalAddr().String(),
		"bytes_transferred": nBytes,
		"transport":         l.transport,
	}).Info(msg)
}

func (l *loggingConnection) ReadFrom(p []byte) (n int, addr net.Addr, err error) {
	udpConn, ok := l.Conn.(*net.UDPConn)
	if !ok {
		return 0, nil, fmt.Errorf("underlying DNS resolution not using UDP")
	}
	n, addr, err = udpConn.ReadFrom(p)
	l.emitLog(n, "bytes read from connection")
	return n, addr, err
}

func (l *loggingConnection) WriteTo(p []byte, addr net.Addr) (n int, err error) {
	udpConn, ok := l.Conn.(*net.UDPConn)
	if !ok {
		return 0, fmt.Errorf("underlying DNS resolution not using UDP")
	}
	n, err = udpConn.WriteTo(p, addr)
	l.emitLog(n, "bytes written to connection")
	return n, err
}

func (l *loggingConnection) Read(b []byte) (n int, err error) {
	n, err = l.Conn.Read(b)
	l.emitLog(n, "bytes read from connection")
	return n, err
}

func (l *loggingConnection) Write(b []byte) (n int, err error) {
	n, err = l.Conn.Write(b)
	l.emitLog(n, "bytes written to connection")
	return n, err
}

func (l *loggingConnection) Close() error {
	return l.Conn.Close()
}

func (l *loggingConnection) LocalAddr() net.Addr {
	return l.Conn.LocalAddr()
}

func (l *loggingConnection) RemoteAddr() net.Addr {
	return l.Conn.RemoteAddr()
}

func (l *loggingConnection) SetDeadline(t time.Time) error {
	return l.Conn.SetDeadline(t)
}

func (l *loggingConnection) SetReadDeadline(t time.Time) error {
	return l.Conn.SetReadDeadline(t)
}

func (l *loggingConnection) SetWriteDeadline(t time.Time) error {
	return l.Conn.SetWriteDeadline(t)
}
